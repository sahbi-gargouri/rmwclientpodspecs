
Pod::Spec.new do |s|

  s.platform = :ios
  s.ios.deployment_target = '8.0'
  s.name         = "RmwApiClient"
  s.version      = "0.1.2"
  s.summary      = "RmwApiClient facilitate RMW API calls."
  s.homepage     = "http://google.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "GARGOURI Sahbi" => "sahbi.gargouri@externe.bnpparibas.com" }
  s.source       = { :git => "https://sahbi-gargouri@bitbucket.org/sahbi-gargouri/rmwapiclient.git", :tag => "0.1.2" }
  s.source_files  = "RmwApiClient/**/*.{h,m}", "*.{h,m}" , "Service/*.{h,m}"
  s.dependency "AFNetworking", "~> 2.5"


end
